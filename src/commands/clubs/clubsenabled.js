const { Command } = require('discord-akairo')
const discord = require('discord.js')
// const { Message } = discord
const fs = require("fs")
const log = require('winston')
const load = path => JSON.parse(fs.readFileSync(path, "utf-8"))
function tm (ms) {
  return new Promise(function (resolve) {
    setTimeout(resolve, ms)
  })
}
class JoinCommand extends Command {
    constructor () {
      super('clubsenabled', {
        aliases: ['clubsenabled'],
        description: { content: 'Enables or disables clubs.' },
        userPermissions: ["ADMINISTRATOR"],
        args: [{
          id: 'enabled',
          type: "string"
        }]
      })
    }
    /**
      * @param {Message} message The message the user sent
      */
    async exec (message, { enabled }) {
      if (
        !enabled ||
        (
          !(["true", "yes", "y", "ye"].includes(enabled.toLowerCase())) &&
          !(["false", "no", "n", "ye"].includes(enabled.toLowerCase()))
        )
      ) {
        return message.channel.send(
          new discord.RichEmbed()
          .setTitle("Incorrect usage")
          .setColor(0x6b3fa0)
          .setDescription("Usage: `!clubsenabled true/false`")
        )
      }
   
      var truthy = ["true", "yes", "y", "ye"].includes(enabled.toLowerCase())
      fs.writeFileSync("./data/clubsenabled", truthy ? "true" : "false")
      return message.channel.send(
        new discord.RichEmbed()
        .setTitle("Done!")
        .setColor(0x3b3f80)
        .setDescription("You're welcome xo")
      )
    }
}
   
module.exports = JoinCommand