var economy = require("./economy")
var logger = require("winston")
module.exports = async (client) => {
    var bubblegum = client.guilds.get("334629377440481280")
    bubblegum.channels
        .filter( c => c.type === "voice" )
        .forEach(async c => {
            c.members.forEach(async m => {
                try { setTimeout( () => economy.award(m.id, Math.floor((7000 / 60) / 60)), Math.floor(Math.random() * 20000)) }
                catch (e) { logger.debug(e) }
            })
        })
}