FROM node:carbon
VOLUME /bot
COPY package.json /bot/
WORKDIR /bot
RUN npm i
ENV TINI_VERSION v0.18.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]
CMD node main.js 
